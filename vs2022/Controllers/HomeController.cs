﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using vs2022.Models;

namespace vs2022.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(LoginViewModel userDetails)
        {
            if (ModelState.IsValid)
            {
                var userId = userDetails.UserName.ToLower() == "zane" ? 1 : 2;
                Response.Cookies.Append("userId", $"{userId}");

                return RedirectToAction("Index", "Products");
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}