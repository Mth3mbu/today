﻿using Microsoft.AspNetCore.Mvc;
using vs2022.Services;

namespace vs2022.Controllers
{
    public class ProductsController : Controller
    {

        private readonly IProductService _productService;
        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: ProductsController
        public async Task<ActionResult> Index()
        {
            var products = await _productService.GetAllProducts();

            return View(products);
        }
    }
}
