﻿using vs2022.Models;

namespace vs2022.Services
{
    public interface IOrderService
    {
        Task<bool> PlaceOrder(Order order);
    }
}
