﻿using vs2022.Models;

namespace vs2022.Services
{
    public interface IProductService
    {
        Task<ProductViewModel> GetAllProducts();
    }
}
